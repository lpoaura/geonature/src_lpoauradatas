DELETE
FROM import_vn.observations_json
WHERE
    site = 'test';
INSERT INTO
    import_vn.observations_json (id, site, item, update_ts)
SELECT id, 'test', item, update_ts
FROM import_vn.observations_json
LIMIT 100
ON CONFLICT DO NOTHING;
SELECT count(*)
FROM src_lpodatas.observations;
EXPLAIN (ANALYZE, VERBOSE)
    SELECT
        o.site                                                                                                                   AS site
      ,
        o.id                                                                                                                     AS id
      ,
        CAST(o.item #>> '{species,@id}' AS INTEGER)                                                                              AS source_id_sp
      ,
        src_lpodatas.get_taxref_values_from_vn('cd_nom'::TEXT,
                                               CAST(o.item #>> '{species,@id}' AS INTEGER))                                      AS taxref_cdnom
      ,
        o.item #>> '{species,taxonomy}'                                                                                          AS groupe_taxo
      ,
        src_lpodatas.get_taxref_values_from_vn('group1_inpn'::TEXT,
                                               CAST(o.item #>> '{species,@id}' AS INTEGER))                                      AS group1_inpn
      ,
        src_lpodatas.get_taxref_values_from_vn('group2_inpn'::TEXT,
                                               CAST(o.item #>> '{species,@id}' AS INTEGER))                                      AS group2_inpn
      ,
        src_lpodatas.get_taxref_values_from_vn('cd_nom'::TEXT,
                                               CAST(o.item #>> '{species,@id}' AS INTEGER))                                      AS taxon_vrai
      ,
        CASE
            WHEN src_lpodatas.get_taxref_values_from_vn('cd_nom'::TEXT,
                                                        CAST(o.item #>> '{species,@id}' AS INTEGER)) IS NOT NULL
                THEN split_part(
                    src_lpodatas.get_taxref_values_from_vn('nom_vern'::TEXT,
                                                           CAST(o.item #>> '{species,@id}' AS INTEGER)),
                    ',', 1)
            ELSE src_lpodatas.get_species_values_from_vn('french_name'::TEXT,
                                                         CAST(o.item #>> '{species,@id}' AS INTEGER))
            END                                                                                                                  AS nom_vern
      ,
        CASE
            WHEN src_lpodatas.get_taxref_values_from_vn('cd_nom'::TEXT,
                                                        CAST(o.item #>> '{species,@id}' AS INTEGER)) IS NOT NULL
                THEN split_part(
                    src_lpodatas.get_taxref_values_from_vn('lb_nom'::TEXT,
                                                           CAST(o.item #>> '{species,@id}' AS INTEGER)),
                    ',', 1)
            ELSE src_lpodatas.get_species_values_from_vn('latin_name'::TEXT,
                                                         CAST(o.item #>> '{species,@id}' AS INTEGER))
            END                                                                                                                  AS nom_sci
      ,
        src_lpodatas.get_observer_full_name_from_vn(
                cast(((o.item -> 'observers') -> 0) ->> '@uid' AS INTEGER))                                                      AS observateur
      ,
        encode(hmac((((o.item -> 'observers') -> 0) ->> '@uid')::TEXT, '$(db_secret_key)', 'sha1'),
               'hex')                                                                                                            AS pseudo_observer_uid
      ,
        CAST(((o.item -> 'observers') -> 0) ->> 'atlas_code' AS INTEGER)                                                         AS oiso_code_nidif
      ,
        ref_nomenclatures.get_nomenclature_label_from_id('VN_ATLAS_CODE',
                                                         CAST(((o.item -> 'observers') -> 0) ->> 'atlas_code' AS INTEGER)::TEXT) AS oiso_statut_nidif
      ,
        CAST(((o.item -> 'observers') -> 0) ->> 'count' AS INTEGER)                                                              AS nombre_total
      ,
        ((o.item -> 'observers') -> 0) ->> 'estimation_code'                                                                     AS code_estimation
      ,
        to_date(o.item #>> '{date,@ISO8601}', 'YYYY-MM-DD')                                                                      AS date
      ,
        CAST(
                extract(YEAR FROM
                        to_date(o.item #>> '{date,@ISO8601}', 'YYYY-MM-DD')) AS INTEGER)                                         AS date_an
      ,
        CAST(((o.item -> 'observers') -> 0) ->> 'altitude' AS INTEGER)                                                           AS altitude
      ,
        CAST(
                ((((o.item -> 'observers') -> 0) #>> '{extended_info,mortality}'::TEXT[]) IS NOT NULL) AS BOOLEAN)               AS mortalite
      ,
        ((o.item -> 'observers') -> 0) #>>
        '{extended_info, mortality, death_cause2}'                                                                               AS mortalite_cause
      ,
        st_transform(ST_SetSRID(ST_MakePoint(
                                        CAST(((o.item -> 'observers') -> 0) ->> 'coord_lon' AS FLOAT),
                                        CAST(((o.item -> 'observers') -> 0) ->> 'coord_lat' AS FLOAT)),
                                4326),
                     2154)                                                                                                       AS geom
      ,
        FALSE                                                                                                                    AS exp_excl
      ,
        ((o.item -> 'observers') -> 0) ->> 'project_code'                                                                        AS code_etude
      ,
        ((o.item -> 'observers') -> 0) ->> 'comment'                                                                             AS commentaires
      ,
        --         commentaires  string_agg(r.comment, ' | ')
        --                            FROM (SELECT 1                                                                  AS id,
        --                                         ((CAST(o.item->>0 AS JSON) -> 'observers') -> 0) ->> 'comment'::text AS comment
        --                                  UNION
        --                                  SELECT 2
        --                                       , 'COMMENTAIRE PRIVE: ' ||
        --                                          (cast((CAST(o.item->>0 AS JSON) -> 'observers') -> 0 as json)) ->> 'hidden_comment'::text) AS r
        --                            ORDER BY r.id,
        NULL                                                                                                                     AS pers_morale
      ,
        to_timestamp(update_ts)                                                                                                  AS derniere_maj
      ,
        NULL                                                                                                                     AS comportement
      ,
        ((o.item -> 'observers') -> 0) ->> 'precision'                                                                           AS "precision"
      ,
        NULL                                                                                                                     AS details
      ,
        --         details  ((o.item -> 'observers') -> 0) ->> 'details',
        p.item ->> 'name'                                                                                                        AS place
      ,
        NULL                                                                                                                     AS formulaire
      ,
        NULL                                                                                                                     AS id_formulaire
    FROM
        import_vn.observations_json o
      ,
        import_vn.places_json p
    WHERE
            (p.id, p.site) = (CAST(o.item #>> '{place,@id}' AS INTEGER), o.site) AND
            o.id = 1;
WITH t1 AS (
    SELECT
        id
      ,
        (((item -> 'observers') -> 0) -> 'details') -> 0 AS item
        --        json_each(cast(((item -> 'observers') -> 0) ->> 'details'-> 0 AS json))
    FROM import_vn.observations_json
    WHERE ((item -> 'observers') -> 0) ->> 'details' IS NOT NULL
    LIMIT 10);

INSERT INTO
    import_vn.observations_json (id, site, item, update_ts, id_form_universal)
SELECT id, 'test', item, update_ts, id_form_universal
FROM import_vn.observations_json
LIMIT 1000;
SELECT source, count(*)
FROM src_lpodatas.observations
GROUP BY
    source;

-- TESTS --
UPDATE import_vn.increment_log
SET
    last_ts = (SELECT max(download_ts)
               FROM import_vn.download_log
               WHERE download_ts::DATE <= DATE '2019-08-19');

DELETE
FROM import_vn.uuid_xref
WHERE
        (site, id) IN (SELECT site, id
                       FROM import_vn.observations_json
                       WHERE
                               to_timestamp(update_ts) >= (SELECT max(download_ts)
                                                           FROM import_vn.download_log
                                                           WHERE download_ts::DATE <= DATE '2019-08-19'))

DELETE
FROM import_vn.observations_json
WHERE
        to_timestamp(update_ts) >= (SELECT max(download_ts)
                                    FROM import_vn.download_log
                                    WHERE download_ts::DATE <= DATE '2019-08-19');
;

TRUNCATE src_lpodatas.observations RESTART IDENTITY;

SELECT *
FROM src_lpodatas.observations;

SELECT
    o.site         AS o_site
  ,
    u.site         AS u_site
  ,
    o.id_sighting  AS o_id
  ,
    u.id           AS u_id
  ,
    o.id_universal AS o_uid
  ,
    u.universal_id AS u_uid
  ,
    o.update_date
FROM
    src_vn.observations o
        LEFT JOIN import_vn.uuid_xref u ON (o.site, o.id_sighting) = (u.site, u.id)
WHERE
    u.uuid IS NULL
ORDER BY
    o.update_date DESC;

SELECT
    o.site                                            AS obs_site
  ,
    u.site                                            AS uuid_site
  ,
    o.id                                              AS obs_id
  ,
    u.id                                              AS uuid_id
  ,
    ((o.item -> 'observers') -> 0) ->> 'id_universal' AS obs_uid
  ,
    u.universal_id                                    AS uuid_uid
  ,
    to_timestamp(o.update_ts)
FROM
    import_vn.observations_json o
        LEFT JOIN import_vn.uuid_xref u ON (o.site, o.id) = (u.site, u.id)
WHERE
    u.uuid IS NULL
ORDER BY
    to_timestamp(o.update_ts) DESC;

WITH t1 AS (
    SELECT
        o.site                                                     AS obs_site
      ,
        u.site                                                     AS uuid_site
      ,
        o.id                                                       AS obs_id
      ,
        u.id                                                       AS uuid_id
      ,
        ((o.item -> 'observers') -> 0) ->> 'id_universal'          AS obs_uid
      ,
        u.universal_id                                             AS uuid_uid
      ,
        o.site
      ,
        o.id
      ,
        o.id_form_universal
      ,
        CAST(((o.item -> 'observers') -> 0) ->> '@uid' AS INTEGER) AS observer_uid
      ,
        to_timestamp(o.update_ts)
    FROM
        import_vn.observations_json o
            LEFT JOIN import_vn.uuid_xref u ON (o.site, o.id) = (u.site, u.id)
    WHERE u.uuid IS NULL
    ORDER BY to_timestamp(o.update_ts) DESC),
     t2 AS (WITH t21 AS (SELECT
                             observers.id_universal
                           ,
                             count(observations.*)
                           ,
                             observers.name || ' ' || observers.surname AS name
                         FROM
                             src_vn.observers
                                 JOIN src_vn.observations ON observers.id_universal = observations.observer_uid
                         GROUP BY observers.id_universal, observers.name || ' ' || observers.surname
                         ORDER BY count(observations.*) DESC)
            SELECT row_number() OVER () AS rank, id_universal, name, count
            FROM t21
     )
SELECT observer_uid, t2.name, t2.rank, count(*)
FROM
    t1
        LEFT JOIN src_vn.observers obser ON t1.observer_uid = obser.id_universal
        LEFT JOIN t2 ON t1.observer_uid = t2.id_universal
GROUP BY
    observer_uid
  , t2.name
  , t2.rank
ORDER BY
    count(*) DESC

SELECT *
FROM src_vn.observations
WHERE
    observer_uid = 91233

SELECT count(*)
FROM
;
SELECT count(DISTINCT ((item -> 'observers') -> 0) ->> 'id_universal')
FROM import_vn.observations_json;
SELECT count(*)
FROM import_vn.uuid_xref;
SELECT count(*)
FROM import_vn.observations_json;

SELECT count(DISTINCT id_form_universal)
FROM src_vn.observations;
SELECT protocol, count(*)
FROM src_vn.forms
GROUP BY
    protocol;


SELECT count(DISTINCT id_form_universal)
FROM src_vn.observations;
SELECT protocol, count(*)
FROM src_vn.forms
GROUP BY
    protocol
;
SELECT jsonb_pretty(item) AS protocol
FROM import_vn.forms_json;
SELECT jsonb_pretty(item)
FROM import_vn.forms_json
WHERE
    item::TEXT ILIKE '%"full_form": "1"%'
LIMIT 100 WHERE

SELECT jsonb_pretty(item)
FROM import_vn.observations_json
WHERE
        CAST(extract(YEAR FROM to_date(item #>> '{date,@timestamp}', 'YYYY-MM-DD')) AS INTEGER) = 2018;

SELECT jsonb_pretty(item)
FROM import_vn.observations_json
LIMIT 100;
SELECT
    to_date((item #>> '{date,@timestamp}')::INT)
        AS integer
FROM import_vn.observations_json
LIMIT 100;


SELECT
        '{
          "protocol": {
            "habitat": {
              "hp1": "E0_E",
              "hp2": "E1_3",
              "hp3A": "E2_1",
              "hp3B": "E2_2",
              "hp4A": "E3_5",
              "hp4B": "E3_3"
            },
            "advanced": "0",
            "site_code": "70613",
            "stoc_rain": "NO_RAIN",
            "stoc_snow": "NO_SNOW",
            "stoc_wind": "WEAK_WIND",
            "stoc_cloud": "TWO_THIRD",
            "visit_number": "3",
            "protocol_name": "STOC_EPS",
            "local_site_code": "Saint-Cierge-la-Serre",
            "sequence_number": "3",
            "stoc_visibility": "GOOD_VISIBILITY"
          }
        }'::JSONB #>> '{protocol, protocol_name}'

SELECT site, id, src_vn.behaviour_array(((NEW.item -> 'observers') -> 0) -> 'behaviours')
FROM import_vn.observations_json new
WHERE
        array_length(src_vn.behaviour_array(((NEW.item -> 'observers') -> 0) -> 'behaviours'), 1) > 1;
SELECT *
FROM (SELECT ARRAY ['134_10','134_21']) t;

CREATE OR REPLACE FUNCTION
    select *, item ->> 'text', jsonb_pretty(item) FROM import_vn.field_details_json WHERE id IN (SELECT unnest(ARRAY ['134_10','134_21']));
SELECT src_lpodatas.get_behaviours_texts_array_from_id_array(ARRAY ['134_10','134_21']);

SELECT jsonb_pretty(item)
FROM import_vn.observations_json
WHERE
    id = 363952;

CREATE INDEX ON import_vn.entities_json(site);
EXPLAIN (VERBOSE, ANALYZE ) SELECT usr.item ->> 'id_entity' AS id_entity, ent.item ->> 'short_name'
                            FROM
                                import_vn.observers_json usr
                                    JOIN import_vn.entities_json ent
                                         ON (usr.site, cast(usr.item ->> 'id_entity' AS INT)) = (ent.site, ent.id)
                            WHERE
                                usr.id_universal = 46274 AND
                                usr.site = 'vn07';
SELECT jsonb_pretty(item)
FROM import_vn.entities_json
WHERE
    id = 3;
SELECT *
FROM


    SET TIMEZONE = 'Europe/Paris';

TRUNCATE src_lpodatas.observations RESTART IDENTITY;
EXPLAIN (VERBOSE, ANALYZE) UPDATE import_vn.observations_json
                           SET
                               site = site
                           WHERE
                                   (site, id) IN (SELECT site, id
                                                  FROM import_vn.observations_json
                                                  LIMIT 1000);

SELECT
    schema_name
  ,
    pg_size_pretty(sum(table_size)::BIGINT)
  ,
    TRUNC((sum(table_size) / pg_database_size(current_database())) * 100)
FROM
    (
        SELECT
            pg_catalog.pg_namespace.nspname           AS schema_name
          ,
            pg_relation_size(pg_catalog.pg_class.oid) AS table_size
        FROM
            pg_catalog.pg_class
                JOIN pg_catalog.pg_namespace ON relnamespace = pg_catalog.pg_namespace.oid
    ) t
GROUP BY
    schema_name
ORDER BY
    schema_name;

SELECT
    relname                                       AS "relation"
  ,
    pg_size_pretty(pg_total_relation_size(C.oid)) AS "total_size"
FROM
    pg_class C
        LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
WHERE
    nspname NOT IN ('pg_catalog', 'information_schema') AND
    C.relkind <> 'i' AND
    nspname !~ '^pg_toast' AND
    nspname LIKE 'data'
ORDER BY
    pg_total_relation_size(C.oid) DESC;
--   LIMIT 5;

WITH datavn AS (
    SELECT
        obs.source_id_sp                        AS vn_id
      , obs.nom_sci
      , obs.nom_vern
      , count(obs.uuid)                         AS nb_donnee
      , count(DISTINCT obs.pseudo_observer_uid) AS nb_observateur
      , max(sn.code_nidif)                      AS max_atlas_code
      , max(date_an) :: INT                     AS derniere_obs
    FROM
        src_lpodatas.observations AS obs
            LEFT JOIN referentiel.statut_nidif sn
                      ON obs.oiso_code_nidif = sn.code_repro
      ,
        zone_etude.zone_etude AS ze
    WHERE
        obs.nombre_total > 0 AND
        ze.id = {id} AND st_within(obs.geom, ze.geom)
--     WHERE obs.nombre_total > 0 AND ze.id = 182 AND st_within(obs.geom, ze.geom)
    GROUP BY obs.source_id_sp
      , obs.nom_sci
      , obs.nom_vern),
     synth AS (
         SELECT DISTINCT
             d.vn_id
           , vntr.taxref_id
           , vntr.vn_grouptax
           , CASE WHEN vntr.rang IN ('es', 'sses')
                      THEN 'Esp. vraie'
                  ELSE NULL END       AS espece
           , CASE WHEN vntr.taxref_nom_vern IS NOT NULL
                      THEN vntr.taxref_nom_vern
                  ELSE d.nom_vern END AS nom_francais
           , CASE WHEN vntr.taxref_nom_sci IS NOT NULL
                      THEN vntr.taxref_nom_sci
                  ELSE d.nom_sci END  AS nom_scientifique
           , d.nb_donnee
           , d.nb_observateur
           , d.derniere_obs
           , sn2.statut_nidif
         FROM
             datavn d
                 LEFT JOIN referentiel.corresp_vn_taxref vntr
                           ON vntr.vn_id = d.vn_id
                 LEFT JOIN referentiel.statut_nidif sn2
                           ON d.max_atlas_code = sn2.code_nidif
         ORDER BY vntr.vn_grouptax, d.vn_id)
SELECT
            row_number()
            OVER () AS id
  ,         s.*
FROM synth AS s;

SELECT DISTINCT
            row_number()
            OVER ()                                                                    AS id
  ,         obs.groupe_taxo                                                            AS Groupe_taxonomique
  ,         count(DISTINCT obs.source_id_sp) FILTER (WHERE vt.rang IN ('es', 'sses') ) AS Taxons
  ,         count(DISTINCT obs.observateur)                                            AS Observateurs
  ,         count(obs.uuid)                                                            AS Observations
FROM
    src_lpodatas.observations obs
        LEFT JOIN referentiel.corresp_vn_taxref vt
                  ON obs.source_id_sp = vt.vn_id
  , zone_etude.zone_etude z
WHERE
    z.id = 182 AND
    st_intersects(obs.geom, z.geom)
GROUP BY
    groupe_taxo
ORDER BY
    count(obs.uuid) DESC;

UPDATE src_lpodatas.observations
SET
    date        = to_timestamp(CAST(obs.item #>> '{date,@timestamp}' AS DOUBLE PRECISION))
  , date_an     = CAST(
        extract(YEAR FROM to_timestamp(CAST(obs.item #>> '{date,@timestamp}' AS DOUBLE PRECISION))) AS INTEGER)
  , place       = obs.item #>> '{place,name}'
  , groupe_taxo = tax.item ->> 'name'
FROM import_vn.observations_json obs, import_vn.taxo_groups_json tax
WHERE
        (observations.source, observations.source_id_data) = (obs.site, obs.id) AND
        (obs.site, cast(obs.item #>> '{species,taxonomy}' AS INTEGER)) = (tax.site, tax.id) AND
        obs.site LIKE 'vn07' AND
        observations.source LIKE 'vn07';

SELECT *
FROM import_vn.observations_json
LIMIT 10;

SELECT
    to_timestamp(CAST(obs.item #>> '{date,@timestamp}' AS DOUBLE PRECISION))                 AS date
  , CAST(
            extract(YEAR FROM to_timestamp(
                    CAST(obs.item #>> '{date,@timestamp}' AS DOUBLE PRECISION))) AS INTEGER) AS date_an
  , obs.item #>> '{place,name}'                                                              AS place
  , tax.item ->> 'name'                                                                      AS grouptax
FROM import_vn.observations_json obs, import_vn.taxo_groups_json tax
WHERE
        (obs.site, cast(obs.item #>> '{species,taxonomy}' AS INTEGER)) = (tax.site, tax.id)
LIMIT 100;
SELECT groupe_taxo, count(*)
FROM src_lpodatas.observations AS vn, zone_etude.zone_etude AS ze
WHERE
    st_within(vn.geom, ze.geom) AND
    ze.id = {}
GROUP BY groupe_taxo
ORDER BY count(*) DESC;

SET ROLE lpo42;
RESET ROLE;

SELECT groupe_taxo, count(*)
FROM src_lpodatas.observations AS vn, zone_etude.zone_etude AS ze
WHERE
    st_within(vn.geom, ze.geom) AND
    ze.id = 182
GROUP BY
    groupe_taxo
ORDER BY
    count(*) DESC;
SET ROLE lpo42;
SELECT groupe_taxo, count(*)
                FROM src_lpodatas.observations AS vn, zone_etude.zone_etude AS ze
                WHERE
                    st_within(vn.geom, ze.geom) AND
                    ze.id = 182
                GROUP BY
                    groupe_taxo
                ORDER BY
                    count(*) asc;