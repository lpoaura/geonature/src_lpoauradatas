-- Trigger function to add or update geometry
DROP FUNCTION IF EXISTS src_lpodatas.update_geom_triggerfn;
CREATE OR REPLACE FUNCTION src_lpodatas.update_geom_triggerfn()
    RETURNS TRIGGER AS
$body$
BEGIN
    new.geom := ST_SetSRID(ST_MakePoint(new.coord_x_local, new.coord_y_local), 2154);
    RETURN new;
END;
$body$
    LANGUAGE plpgsql;

-- Trigger function to get taxref values from visionature id_species
DROP FUNCTION IF EXISTS src_lpodatas.get_observation_uuid;
CREATE OR REPLACE FUNCTION src_lpodatas.get_observation_uuid(site VARCHAR, id INT, OUT uuid UUID)
    RETURNS UUID AS
$$
BEGIN
    EXECUTE format(
            'SELECT uuid from import_vn.uuid_xref where site like $1 and id = $2 limit 1')
        INTO uuid
        USING site, id;
END ;
$$ LANGUAGE plpgsql;


-- Trigger function to get taxref values from visionature id_species
DROP FUNCTION IF EXISTS src_lpodatas.get_taxref_values_from_vn;
CREATE OR REPLACE FUNCTION src_lpodatas.get_taxref_values_from_vn(field_name ANYELEMENT, id_species INT, OUT result ANYELEMENT)
    RETURNS ANYELEMENT AS
$$
BEGIN
    EXECUTE format(
            'SELECT taxref.%I from taxonomie.cor_vn_taxref join taxonomie.taxref on cor_vn_taxref.taxref_id = taxref.cd_nom where vn_id = $1 limit 1',
            field_name)
        INTO result
        USING id_species;
END ;
$$ LANGUAGE plpgsql;

SELECT src_lpodatas.get_taxref_values_from_vn('nom_vern'::TEXT, 3);

DROP FUNCTION IF EXISTS src_lpodatas.get_species_values_from_vn;
CREATE OR REPLACE FUNCTION src_lpodatas.get_species_values_from_vn(field_name ANYELEMENT, id_species INT, OUT result ANYELEMENT)
    RETURNS ANYELEMENT AS
$$
BEGIN
    EXECUTE 'select item ->> $1 from import_vn.species_json where species_json.id = $2 limit 1;'
        INTO result
        USING field_name, id_species;
END ;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS src_lpodatas.get_taxo_group_name_from_vn;
CREATE OR REPLACE FUNCTION src_lpodatas.get_taxo_group_values_from_vn(key TEXT, site TEXT, id INTEGER, OUT result TEXT)
    RETURNS TEXT AS
$$
BEGIN
    EXECUTE 'select item ->> $1 from import_vn.taxo_groups_json where taxo_groups_json.id = $3 and taxo_groups_json.site like $2 limit 1;'
        INTO result
        USING key, site, id;
END ;
$$ LANGUAGE plpgsql;



DROP FUNCTION IF EXISTS src_lpodatas.get_taxon_names_from_vn;
CREATE OR REPLACE FUNCTION src_lpodatas.get_taxon_names_from_vn(field_name ANYELEMENT, id_species INT, OUT result ANYELEMENT)
    RETURNS ANYELEMENT AS
$$
BEGIN
    EXECUTE format(
            'SELECT taxref.%I from taxonomie.cor_vn_taxref join taxonomie.taxref on cor_vn_taxref.taxref_id = taxref.cd_nom where vn_id = $1 limit 1',
            field_name)
        INTO result
        USING id_species;
END ;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS src_lpodatas.get_observer_full_name_from_vn;
CREATE OR REPLACE FUNCTION src_lpodatas.get_observer_full_name_from_vn(id_universal INT, OUT result TEXT)
    RETURNS TEXT AS
$$
BEGIN
    EXECUTE
        format(
                'select concat(UPPER(item ->> ''name''), '' '', item ->> ''surname'') as text from import_vn.observers_json where observers_json.id_universal = $1 limit 1')
        INTO result
        USING id_universal;
END ;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS ref_nomenclatures.get_nomenclature_label_from_id;
CREATE OR REPLACE FUNCTION ref_nomenclatures.get_nomenclature_label_from_id(mytype CHARACTER VARYING, mycdnomenclature TEXT, OUT result TEXT)
    RETURNS TEXT AS
$$
BEGIN
    SELECT INTO result label_default
    FROM ref_nomenclatures.t_nomenclatures n
    WHERE
        n.id_type = ref_nomenclatures.get_id_nomenclature_type(mytype) AND
        mycdnomenclature = n.cd_nomenclature;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS src_lpodatas.get_behaviours_texts_array_from_id_array;
CREATE OR REPLACE FUNCTION src_lpodatas.get_behaviours_texts_array_from_id_array(array_id TEXT[], OUT result TEXT[])
    RETURNS TEXT[] AS
$$
BEGIN
    SELECT INTO result array_agg(item ->> 'text')
    FROM import_vn.field_details_json
    WHERE id IN (SELECT unnest(array_id));
END;
$$ LANGUAGE plpgsql;


DROP FUNCTION IF EXISTS src_lpodatas.get_entity_from_observer_site_uid;
CREATE OR REPLACE FUNCTION src_lpodatas.get_entity_from_observer_site_uid(observer_uid INTEGER, siteref TEXT, OUT result TEXT)
    RETURNS TEXT AS
$$
BEGIN
    SELECT INTO result ent.item ->> 'short_name'
    FROM
        import_vn.observers_json usr
            JOIN import_vn.entities_json ent ON (usr.site, cast(usr.item ->> 'id_entity' AS INT)) = (ent.site, ent.id)
    WHERE
        usr.id_universal = observer_uid AND
        usr.site = siteref;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION src_lpodatas.obsfull_delete_from_vn() RETURNS TRIGGER AS
$$
BEGIN
    RAISE NOTICE 'TG_OP %', tg_op;
    -- Deleting data on src_vn.observations when raw data is deleted
    DELETE
    FROM src_lpodatas.observations
    WHERE
        source_id_data = old.id AND
        source = old.site;
    RAISE NOTICE 'DELETE DATA % from %', old.id, old.site;
    IF NOT found
    THEN
        RETURN NULL;
    END IF;
    RETURN old;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION src_lpodatas.obsfull_upsert_from_vn() RETURNS TRIGGER AS
$$
DECLARE
    the_uuid                UUID;
    the_source              VARCHAR(15);
    the_source_id_data      INT;
    the_source_id_sp        INT;
    the_taxref_cdnom        INT;
    the_groupe_taxo         VARCHAR(50);
    the_group1_inpn         VARCHAR(50);
    the_group2_inpn         VARCHAR(50);
    the_taxon_vrai          BOOL;
    the_nom_vern            VARCHAR(250);
    the_nom_sci             VARCHAR(250);
    the_observateur         VARCHAR(250);
    the_pseudo_observer_uid VARCHAR(200);
    the_oiso_code_nidif     INT;
    the_oiso_statut_nidif   VARCHAR(20);
    the_cs_colo_repro       BOOLEAN;
    the_cs_is_gite          BOOLEAN;
    the_cs_periode          VARCHAR(20);
    the_nombre_total        INT;
    the_code_estimation     VARCHAR(50);
    the_date                DATE;
    the_date_an             INT;
    the_altitude            INTEGER;
    the_mortalite           BOOLEAN;
    the_mortalite_cause     VARCHAR(250);
    the_geom                GEOMETRY(point, 2154);
    the_exp_excl            BOOLEAN;
    the_code_etude          VARCHAR(50);
    the_commentaires        TEXT;
    the_pers_morale         VARCHAR(100);
    the_comportement        TEXT[];
    the_precision           VARCHAR(50);
    the_details             JSONB;
    the_place               VARCHAR(250);
    the_id_formulaire       VARCHAR;
    the_derniere_maj        TIMESTAMP;
BEGIN
    the_uuid = src_lpodatas.get_observation_uuid(new.site, new.id);
    the_source = new.site;
    the_source_id_data = new.id;
    the_source_id_sp = CAST(new.item #>> '{species,@id}' AS INTEGER);
    the_taxref_cdnom = cast(src_lpodatas.get_taxref_values_from_vn('cd_nom'::TEXT, the_source_id_sp) AS INT);
    the_groupe_taxo = src_lpodatas.get_taxo_group_values_from_vn('name', new.site, cast(new.item #>> '{species,taxonomy}' AS INT));
    the_group1_inpn = src_lpodatas.get_taxref_values_from_vn('group1_inpn'::TEXT, the_source_id_sp);
    the_group2_inpn = src_lpodatas.get_taxref_values_from_vn('group2_inpn'::TEXT, the_source_id_sp);
    the_taxon_vrai = bool_and(src_lpodatas.get_taxref_values_from_vn('cd_nom'::TEXT, the_source_id_sp) IS NOT NULL);
    the_nom_vern =
            CASE
                WHEN src_lpodatas.get_taxref_values_from_vn('cd_nom'::TEXT, the_source_id_sp) IS NOT NULL
                    THEN split_part(src_lpodatas.get_taxref_values_from_vn('nom_vern'::TEXT, the_source_id_sp), ',', 1)
                ELSE src_lpodatas.get_species_values_from_vn('french_name'::TEXT, the_source_id_sp)
                END;
    the_nom_sci =
            CASE
                WHEN src_lpodatas.get_taxref_values_from_vn('cd_nom'::TEXT, the_source_id_sp) IS NOT NULL
                    THEN split_part(src_lpodatas.get_taxref_values_from_vn('lb_nom'::TEXT, the_source_id_sp), ',', 1)
                ELSE src_lpodatas.get_species_values_from_vn('latin_name'::TEXT, the_source_id_sp)
                END;
    the_observateur =
            src_lpodatas.get_observer_full_name_from_vn(cast(((new.item -> 'observers') -> 0) ->> '@uid' AS INTEGER));
    the_pseudo_observer_uid =
            encode(hmac((((new.item -> 'observers') -> 0) ->> '@uid')::TEXT, '$(db_secret_key)', 'sha1'), 'hex');
    the_oiso_code_nidif = CAST(((new.item -> 'observers') -> 0) ->> 'atlas_code' AS INTEGER);
    the_oiso_statut_nidif =
            ref_nomenclatures.get_nomenclature_label_from_id('VN_ATLAS_CODE', the_oiso_code_nidif::TEXT);
    the_nombre_total = CAST(((new.item -> 'observers') -> 0) ->> 'count' AS INTEGER);
    the_code_estimation = ((new.item -> 'observers') -> 0) ->> 'estimation_code';
    the_date = to_timestamp(CAST(NEW.item #>> '{date,@timestamp}' AS DOUBLE PRECISION));
    the_date_an = CAST(
            extract(YEAR FROM to_timestamp(CAST(NEW.item #>> '{date,@timestamp}' AS DOUBLE PRECISION))) AS INTEGER);
    the_altitude = CAST(((new.item -> 'observers') -> 0) ->> 'altitude' AS INTEGER);
    the_mortalite =
            CAST(((((new.item -> 'observers') -> 0) #>> '{extended_info,mortality}'::TEXT[]) IS NOT NULL) AS BOOLEAN);
    the_mortalite_cause = ((new.item -> 'observers') -> 0) #>> '{extended_info, mortality, death_cause2}';
    the_geom = st_transform(ST_SetSRID(ST_MakePoint(
                                               CAST(((new.item -> 'observers') -> 0) ->> 'coord_lon' AS FLOAT),
                                               CAST(((new.item -> 'observers') -> 0) ->> 'coord_lat' AS FLOAT)),
                                       4326), 2154);
    the_exp_excl = FALSE;
    the_code_etude = ((new.item -> 'observers') -> 0) ->> 'project_code';
    the_commentaires = CASE WHEN ((new.item -> 'observers') -> 0) ? 'comment' AND
                                 ((new.item -> 'observers') -> 0) ? 'hidden_comment'
                                THEN cast(((new.item -> 'observers') -> 0) ->> 'comment' AS TEXT) ||
                                     ' | PRIVATE COMMENT : ' ||
                                     cast(((new.item -> 'observers') -> 0) ->> 'hidden_comment' AS TEXT)
                            WHEN ((new.item -> 'observers') -> 0) ? 'comment'
                                THEN ((new.item -> 'observers') -> 0) ->> 'comment'::TEXT
                            WHEN ((new.item -> 'observers') -> 0) ? 'hidden_comment'
                                THEN 'PRIVATE COMMENT : ' ||
                                     cast(((NEW.item -> 'observers') -> 0) ->> 'hidden_comment' AS TEXT)
                            ELSE NULL END;
    the_pers_morale = src_lpodatas.get_entity_from_observer_site_uid(
            cast(((new.item -> 'observers') -> 0) ->> '@uid' AS INTEGER), new.site);

    the_comportement = src_lpodatas.get_behaviours_texts_array_from_id_array(
            src_vn.behaviour_array(((NEW.item -> 'observers') -> 0) -> 'behaviours'));
    the_precision = ((NEW.item -> 'observers') -> 0) ->> 'precision';
    the_details = CAST(((NEW.item -> 'observers') -> 0) ->> 'details' AS JSONB);
    the_place = NEW.item #>> '{place,name}';
    the_id_formulaire = new.id_form_universal;
    the_derniere_maj = to_timestamp(NEW.update_ts);
    the_cs_colo_repro = FALSE;
    the_cs_is_gite = FALSE;
    the_cs_periode = NULL;
    IF (tg_op = 'UPDATE')
    THEN
        -- Updating data on src_vn.observations when raw data is updated
        UPDATE src_lpodatas.observations
        SET
            uuid=the_uuid
          , source=the_source
          , source_id_data=the_source_id_data
          , source_id_sp=the_source_id_sp
          , taxref_cdnom=the_taxref_cdnom
          , groupe_taxo = the_groupe_taxo
          , group1_inpn = the_group1_inpn
          , group2_inpn = the_group2_inpn
          , taxon_vrai = the_taxon_vrai
          , nom_vern = the_nom_vern
          , nom_sci = the_nom_sci
          , observateur = the_observateur
          , pseudo_observer_uid = the_pseudo_observer_uid
          , oiso_code_nidif = the_oiso_code_nidif
          , oiso_statut_nidif = the_oiso_statut_nidif
          , cs_colo_repro = the_cs_colo_repro
          , cs_is_gite = the_cs_is_gite
          , cs_periode = the_cs_periode
          , nombre_total = the_nombre_total
          , code_estimation = the_code_estimation
          , date = the_date
          , date_an = the_date_an
          , altitude = the_altitude
          , mortalite = the_mortalite
          , mortalite_cause = the_mortalite_cause
          , geom = the_geom
          , exp_excl = the_exp_excl
          , code_etude = the_code_etude
          , commentaires = the_commentaires
          , pers_morale = the_pers_morale
          , derniere_maj = the_derniere_maj
          , comportement = the_comportement
          , "precision"= the_precision
          , details = the_details
          , place = the_place
          , id_formulaire = the_id_formulaire
        WHERE
            source_id_data = OLD.id AND
            source = OLD.site;
        IF NOT found
        THEN
            INSERT INTO
                src_lpodatas.observations ( uuid, source, source_id_data, source_id_sp, taxref_cdnom, groupe_taxo
                                          , group1_inpn, group2_inpn, taxon_vrai, nom_vern, nom_sci, observateur
                                          , pseudo_observer_uid, oiso_code_nidif, oiso_statut_nidif, cs_colo_repro
                                          , cs_is_gite, cs_periode, nombre_total, code_estimation, date, date_an
                                          , altitude, mortalite, mortalite_cause, geom, exp_excl, code_etude
                                          , commentaires, pers_morale, derniere_maj, comportement, "precision", details
                                          , place, id_formulaire)
            VALUES
            ( the_uuid, the_source, the_source_id_data, the_source_id_sp, the_taxref_cdnom, the_groupe_taxo
            , the_group1_inpn, the_group2_inpn, the_taxon_vrai, the_nom_vern, the_nom_sci, the_observateur
            , the_pseudo_observer_uid, the_oiso_code_nidif, the_oiso_statut_nidif, the_cs_colo_repro, the_cs_is_gite
            , the_cs_periode, the_nombre_total, the_code_estimation, the_date, the_date_an, the_altitude, the_mortalite
            , the_mortalite_cause, the_geom, the_exp_excl, the_code_etude, the_commentaires, the_pers_morale
            , the_derniere_maj, the_comportement, the_precision, the_details, the_place, the_id_formulaire);
            RETURN NEW;
        END IF;
        RETURN NEW;
    ELSIF (tg_op = 'INSERT')
    THEN
        -- Inserting data on src_vn.observations when raw data is inserted
        INSERT INTO
            src_lpodatas.observations ( uuid, source, source_id_data, source_id_sp, taxref_cdnom, groupe_taxo
                                      , group1_inpn, group2_inpn, taxon_vrai, nom_vern, nom_sci, observateur
                                      , pseudo_observer_uid, oiso_code_nidif, oiso_statut_nidif, cs_colo_repro
                                      , cs_is_gite, cs_periode, nombre_total, code_estimation, date, date_an, altitude
                                      , mortalite, mortalite_cause, geom, exp_excl, code_etude, commentaires
                                      , pers_morale, derniere_maj, comportement, "precision", details, place
                                      , id_formulaire)
        VALUES
        ( the_uuid, the_source, the_source_id_data, the_source_id_sp, the_taxref_cdnom, the_groupe_taxo, the_group1_inpn
        , the_group2_inpn, the_taxon_vrai, the_nom_vern, the_nom_sci, the_observateur, the_pseudo_observer_uid
        , the_oiso_code_nidif, the_oiso_statut_nidif, the_cs_colo_repro, the_cs_is_gite, the_cs_periode
        , the_nombre_total, the_code_estimation, the_date, the_date_an, the_altitude, the_mortalite, the_mortalite_cause
        , the_geom, the_exp_excl, the_code_etude, the_commentaires, the_pers_morale, the_derniere_maj, the_comportement
        , the_precision, the_details, the_place, the_id_formulaire)
        ON CONFLICT (source, source_id_data)
            DO UPDATE SET
                          uuid=the_uuid
                        , source=the_source
                        , source_id_data=the_source_id_data
                        , source_id_sp=the_source_id_sp
                        , taxref_cdnom=the_taxref_cdnom
                        , groupe_taxo = the_groupe_taxo
                        , group1_inpn = the_group1_inpn
                        , group2_inpn = the_group2_inpn
                        , taxon_vrai = the_taxon_vrai
                        , nom_vern = the_nom_vern
                        , nom_sci = the_nom_sci
                        , observateur = the_observateur
                        , pseudo_observer_uid = the_pseudo_observer_uid
                        , oiso_code_nidif = the_oiso_code_nidif
                        , oiso_statut_nidif = the_oiso_statut_nidif
                        , cs_colo_repro = the_cs_colo_repro
                        , cs_is_gite = the_cs_is_gite
                        , cs_periode = the_cs_periode
                        , nombre_total = the_nombre_total
                        , code_estimation = the_code_estimation
                        , date = the_date
                        , date_an = the_date_an
                        , altitude = the_altitude
                        , mortalite = the_mortalite
                        , mortalite_cause = the_mortalite_cause
                        , geom = the_geom
                        , exp_excl = the_exp_excl
                        , code_etude = the_code_etude
                        , commentaires = the_commentaires
                        , pers_morale = the_pers_morale
                        , derniere_maj = the_derniere_maj
                        , comportement = the_comportement
                        , "precision"= the_precision
                        , details = the_details
                        , place = the_place
                        , id_formulaire = the_id_formulaire
        WHERE
            observations.source_id_data = OLD.id AND
            observations.source = OLD.site;
        RETURN NEW;
        RAISE NOTICE 'NEW %', NEW;
    END IF;
END ;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS obsfull_delete_from_vn_trigger ON import_vn.observations_json;
CREATE TRIGGER obsfull_delete_from_vn_trigger
    AFTER DELETE
    ON import_vn.observations_json
    FOR EACH ROW
EXECUTE PROCEDURE src_lpodatas.obsfull_delete_from_vn();

DROP TRIGGER IF EXISTS obsfull_upsert_from_vn_trigger ON import_vn.observations_json;
CREATE TRIGGER obsfull_upsert_from_vn_trigger
    AFTER INSERT OR UPDATE
    ON import_vn.observations_json
    FOR EACH ROW
EXECUTE PROCEDURE src_lpodatas.obsfull_upsert_from_vn();
