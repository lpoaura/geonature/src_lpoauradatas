-- Trigger function to get taxref values from dbchiro id_species
DROP FUNCTION IF EXISTS src_lpodatas.get_observation_uuid
;

CREATE OR REPLACE FUNCTION src_lpodatas.get_observation_uuid(site VARCHAR, id INT, OUT uuid UUID)
    RETURNS UUID AS
$$
BEGIN
    EXECUTE format(
            'SELECT uuid from import_vn.uuid_xref where site like $1 and id = $2 limit 1')
        INTO uuid
        USING site, id;
END ;
$$
    LANGUAGE plpgsql
;


-- Trigger function to get taxref values from visionature id_species
DROP FUNCTION IF EXISTS src_lpodatas.get_taxref_values_from_dbchiro(field_name ANYELEMENT, codesp_id INT, OUT result ANYELEMENT)
;

CREATE OR REPLACE FUNCTION src_lpodatas.get_taxref_values_from_dbchiro(field_name ANYELEMENT, codesp_id INT, OUT result ANYELEMENT)
    RETURNS ANYELEMENT AS
$$
BEGIN
    EXECUTE format(
            'SELECT %I FROM src_dbchiro.cor_sp_dbchiro_taxref JOIN taxonomie.taxref ON cor_sp_dbchiro_taxref.cdnom_taxref = taxref.cd_nom WHERE cor_sp_dbchiro_taxref.idsp_dbchiro = $1 LIMIT 1;',
            field_name)
        INTO result
        USING codesp_id;
END ;
$$
    LANGUAGE plpgsql
;

-- Trigger function to get session values from dnchjrop id
DROP FUNCTION IF EXISTS src_lpodatas.get_session_values_from_sightings(field_name ANYELEMENT, codesp_id INT, OUT result ANYELEMENT)
;

CREATE OR REPLACE FUNCTION src_lpodatas.get_session_values_from_sightings(field_name ANYELEMENT, session_id INT, OUT result ANYELEMENT)
    RETURNS ANYELEMENT AS
$$
BEGIN
    EXECUTE format(
            'SELECT %I FROM src_dbchiro.sessions WHERE sessions.id_session =  $1 LIMIT 1;',
            field_name)
        INTO result
        USING session_id;
END ;
$$
    LANGUAGE plpgsql
;

-- Trigger function to get place values from dnchjrop id
DROP FUNCTION IF EXISTS src_lpodatas.get_place_values_from_sightings(field_name ANYELEMENT, session_id INT, OUT result ANYELEMENT)
;

CREATE OR REPLACE FUNCTION src_lpodatas.get_place_values_from_sightings(field_name ANYELEMENT, session_id INT, OUT result ANYELEMENT)
    RETURNS ANYELEMENT AS
$$
BEGIN
    EXECUTE format(
            'SELECT places.%I FROM src_dbchiro.places join src_dbchiro.sessions on sessions.place_id = places.id_place WHERE sessions.id_session =  $1 LIMIT 1;',
            field_name)
        INTO result
        USING session_id;
END ;
$$
    LANGUAGE plpgsql
;

-- Trigger function to get place values from dnchjrop id
DROP FUNCTION IF EXISTS src_lpodatas.get_observer_values_from_sightings(field_name ANYELEMENT, observer_id INT, OUT result ANYELEMENT)
;

CREATE OR REPLACE FUNCTION src_lpodatas.get_observer_values_from_sightings(field_name ANYELEMENT, observer_id INT, OUT result ANYELEMENT)
    RETURNS ANYELEMENT AS
$$
BEGIN
    EXECUTE format(
            'SELECT observers.%I FROM src_dbchiro.observers WHERE observers.id = $1 LIMIT 1;',
            field_name)
        INTO result
        USING observer_id;
END ;
$$
    LANGUAGE plpgsql
;


-- Trigger function to get place values from dnchjrop id
DROP FUNCTION IF EXISTS src_lpodatas.get_study_values_from_sightings(field_name ANYELEMENT, session_id INT, OUT result ANYELEMENT)
;

CREATE OR REPLACE FUNCTION src_lpodatas.get_study_values_from_sightings(field_name ANYELEMENT, session_id INT, OUT result ANYELEMENT)
    RETURNS ANYELEMENT AS
$$
BEGIN
    EXECUTE format(
            'SELECT studies.%I FROM src_dbchiro.studies join src_dbchiro.sessions on sessions.study_id = studies.id_study WHERE sessions.id_session =  $1 LIMIT 1;',
            field_name)
        INTO result
        USING session_id;
END ;
$$
    LANGUAGE plpgsql
;

SELECT src_lpodatas.get_study_values_from_sightings('name'::TEXT, 12000)

CREATE OR REPLACE FUNCTION src_lpodatas.obsfull_delete_from_dbchiro() RETURNS TRIGGER AS
$$
BEGIN
    RAISE NOTICE 'TG_OP %', tg_op;
    -- Deleting data on src_vn.observations when raw data is deleted
    DELETE
    FROM src_lpodatas.observations
    WHERE
        source_id_data = old.id_sighting AND
        source = 'dbChiroGCRA';
    RAISE NOTICE 'DELETE DATA % from %', old.id, old.site;
    IF NOT found
    THEN
        RETURN NULL;
    END IF;
    RETURN old;
END;
$$
    LANGUAGE plpgsql
;

CREATE OR REPLACE FUNCTION src_lpodatas.obsfull_upsert_from_dbchiro() RETURNS TRIGGER AS
$$
DECLARE
    the_uuid                UUID;
    the_source              VARCHAR(15);
    the_source_id_data      INT;
    the_source_id_sp        INT;
    the_taxref_cdnom        INT;
    the_groupe_taxo         VARCHAR(50);
    the_group1_inpn         VARCHAR(50);
    the_group2_inpn         VARCHAR(50);
    the_taxon_vrai          BOOL;
    the_nom_vern            VARCHAR(250);
    the_nom_sci             VARCHAR(250);
    the_observateur         VARCHAR(250);
    the_pseudo_observer_uid VARCHAR(200);
    the_oiso_code_nidif     INT;
    the_oiso_statut_nidif   VARCHAR(20);
    the_cs_colo_repro       BOOLEAN;
    the_cs_is_gite          BOOLEAN;
    the_cs_periode          VARCHAR(20);
    the_nombre_total        INT;
    the_code_estimation     VARCHAR(50);
    the_date                DATE;
    the_date_an             INT;
    the_altitude            INTEGER;
    the_mortalite           BOOLEAN;
    the_mortalite_cause     VARCHAR(250);
    the_geom                GEOMETRY(point, 2154);
    the_exp_excl            BOOLEAN;
    the_code_etude          VARCHAR(250);
    the_commentaires        TEXT;
    the_pers_morale         VARCHAR(100);
    the_comportement        TEXT[];
    the_precision           VARCHAR(50);
    the_details             JSONB;
    the_place               VARCHAR(250);
    the_id_formulaire       VARCHAR;
    the_derniere_maj        TIMESTAMP;
BEGIN
    the_uuid = NULL;
    the_source = 'dbChiroGCRA';
    the_source_id_data = new.id_sighting;
    the_source_id_sp = new.codesp_id;
    the_taxref_cdnom = src_lpodatas.get_taxref_values_from_dbchiro('cd_nom'::TEXT, the_source_id_sp);
    the_groupe_taxo = 'Chauves-souris';
    the_group1_inpn = src_lpodatas.get_taxref_values_from_dbchiro('group1_inpn'::TEXT, the_source_id_sp);
    the_group2_inpn = src_lpodatas.get_taxref_values_from_dbchiro('group2_inpn'::TEXT, the_source_id_sp);
    the_taxon_vrai =
            bool_and(src_lpodatas.get_taxref_values_from_dbchiro('cd_nom'::TEXT, the_source_id_sp) IS NOT NULL);
    the_nom_vern =
            CASE
                WHEN src_lpodatas.get_taxref_values_from_dbchiro('cd_nom'::TEXT, the_source_id_sp) IS NOT NULL
                    THEN split_part(src_lpodatas.get_taxref_values_from_dbchiro('nom_vern'::TEXT, the_source_id_sp),
                                    ',', 1)
                ELSE new.common_name
                END;
    the_nom_sci =
            CASE
                WHEN src_lpodatas.get_taxref_values_from_dbchiro('cd_nom'::TEXT, the_source_id_sp) IS NOT NULL
                    THEN split_part(src_lpodatas.get_taxref_values_from_dbchiro('lb_nom'::TEXT, the_source_id_sp), ',',
                                    1)
                ELSE new.sciname
                END;
    the_observateur = src_lpodatas.get_observer_values_from_sightings('first_name'::TEXT, new.observer_id) || ' ' ||
                      src_lpodatas.get_observer_values_from_sightings('last_name'::TEXT, new.observer_id);
    the_pseudo_observer_uid = encode(hmac(new.observer_id::text, '$(db_secret_key)', 'sha1'), 'hex');
    the_oiso_code_nidif = NULL;
    the_oiso_statut_nidif = NULL;
    the_nombre_total = new.total_count;
    the_code_estimation = NULL;
    the_date = cast(src_lpodatas.get_session_values_from_sightings('date_start'::TEXT, new.session_id) as date);
    the_date_an = extract(YEAR FROM the_date);
    the_altitude = src_lpodatas.get_place_values_from_sightings('altitude'::TEXT, new.session_id);
    the_mortalite = NULL;
    the_mortalite_cause = NULL;
    the_geom = src_lpodatas.get_place_values_from_sightings('geom'::TEXT, new.session_id);
    the_exp_excl = FALSE;
    the_code_etude = src_lpodatas.get_study_values_from_sightings('name'::TEXT, new.session_id);
    the_commentaires = new.comment;
    the_pers_morale = src_lpodatas.get_observer_values_from_sightings('organism'::TEXT, new.observer_id);
    the_comportement = NULL;
    the_precision = src_lpodatas.get_place_values_from_sightings('precision'::TEXT, new.session_id);
    the_details = NULL;
    the_place = src_lpodatas.get_place_values_from_sightings('name'::TEXT, new.session_id);
    the_id_formulaire = NULL;
    the_derniere_maj = new.timestamp_update;
    the_cs_colo_repro = new.breed_colo;
    the_cs_is_gite = cast(src_lpodatas.get_place_values_from_sightings('is_gite'::TEXT, new.session_id) AS BOOLEAN);
    the_cs_periode = new.period;
    IF (tg_op = 'UPDATE')
    THEN
        -- Updating data on src_vn.observations when raw data is updated
        UPDATE src_lpodatas.observations
        SET
            uuid=the_uuid
          , source=the_source
          , source_id_data=the_source_id_data
          , source_id_sp=the_source_id_sp
          , taxref_cdnom=the_taxref_cdnom
          , groupe_taxo = the_groupe_taxo
          , group1_inpn = the_group1_inpn
          , group2_inpn = the_group2_inpn
          , taxon_vrai = the_taxon_vrai
          , nom_vern = the_nom_vern
          , nom_sci = the_nom_sci
          , observateur = the_observateur
          , pseudo_observer_uid = the_pseudo_observer_uid
          , oiso_code_nidif = the_oiso_code_nidif
          , oiso_statut_nidif = the_oiso_statut_nidif
          , cs_colo_repro = the_cs_colo_repro
          , cs_is_gite = the_cs_is_gite
          , cs_periode = the_cs_periode
          , nombre_total = the_nombre_total
          , code_estimation = the_code_estimation
          , date = the_date
          , date_an = the_date_an
          , altitude = the_altitude
          , mortalite = the_mortalite
          , mortalite_cause = the_mortalite_cause
          , geom = the_geom
          , exp_excl = the_exp_excl
          , code_etude = the_code_etude
          , commentaires = the_commentaires
          , pers_morale = the_pers_morale
          , derniere_maj = the_derniere_maj
          , comportement = the_comportement
          , "precision"= the_precision
          , details = the_details
          , place = the_place
          , id_formulaire = the_id_formulaire
        WHERE
            source_id_data = old.id_sighting AND
            source = 'dbChiroGCRA';
        IF NOT found
        THEN
            INSERT INTO
                src_lpodatas.observations ( uuid, source, source_id_data, source_id_sp, taxref_cdnom, groupe_taxo
                                          , group1_inpn, group2_inpn, taxon_vrai, nom_vern, nom_sci, observateur
                                          , pseudo_observer_uid, oiso_code_nidif, oiso_statut_nidif, cs_colo_repro
                                          , cs_is_gite, cs_periode, nombre_total, code_estimation, date, date_an
                                          , altitude, mortalite, mortalite_cause, geom, exp_excl, code_etude
                                          , commentaires, pers_morale, derniere_maj, comportement, "precision", details
                                          , place, id_formulaire)
            VALUES
            ( the_uuid, the_source, the_source_id_data, the_source_id_sp, the_taxref_cdnom, the_groupe_taxo
            , the_group1_inpn, the_group2_inpn, the_taxon_vrai, the_nom_vern, the_nom_sci, the_observateur
            , the_pseudo_observer_uid, the_oiso_code_nidif, the_oiso_statut_nidif, the_cs_colo_repro, the_cs_is_gite
            , the_cs_periode, the_nombre_total, the_code_estimation, the_date, the_date_an, the_altitude, the_mortalite
            , the_mortalite_cause, the_geom, the_exp_excl, the_code_etude, the_commentaires, the_pers_morale
            , the_derniere_maj, the_comportement, the_precision, the_details, the_place, the_id_formulaire);
            RETURN new;
        END IF;
        RETURN new;
    ELSIF (tg_op = 'INSERT')
    THEN
        -- Inserting data on src_vn.observations when raw data is inserted
        INSERT INTO
            src_lpodatas.observations ( uuid, source, source_id_data, source_id_sp, taxref_cdnom, groupe_taxo
                                      , group1_inpn, group2_inpn, taxon_vrai, nom_vern, nom_sci, observateur
                                      , pseudo_observer_uid, oiso_code_nidif, oiso_statut_nidif, cs_colo_repro
                                      , cs_is_gite, cs_periode, nombre_total, code_estimation, date, date_an, altitude
                                      , mortalite, mortalite_cause, geom, exp_excl, code_etude, commentaires
                                      , pers_morale, derniere_maj, comportement, "precision", details, place
                                      , id_formulaire)
        VALUES
        ( the_uuid, the_source, the_source_id_data, the_source_id_sp, the_taxref_cdnom, the_groupe_taxo, the_group1_inpn
        , the_group2_inpn, the_taxon_vrai, the_nom_vern, the_nom_sci, the_observateur, the_pseudo_observer_uid
        , the_oiso_code_nidif, the_oiso_statut_nidif, the_cs_colo_repro, the_cs_is_gite, the_cs_periode
        , the_nombre_total, the_code_estimation, the_date, the_date_an, the_altitude, the_mortalite, the_mortalite_cause
        , the_geom, the_exp_excl, the_code_etude, the_commentaires, the_pers_morale, the_derniere_maj, the_comportement
        , the_precision, the_details, the_place, the_id_formulaire)
        ON CONFLICT (source, source_id_data)
            DO UPDATE SET
            uuid=the_uuid
          , source=the_source
          , source_id_data=the_source_id_data
          , source_id_sp=the_source_id_sp
          , taxref_cdnom=the_taxref_cdnom
          , groupe_taxo = the_groupe_taxo
          , group1_inpn = the_group1_inpn
          , group2_inpn = the_group2_inpn
          , taxon_vrai = the_taxon_vrai
          , nom_vern = the_nom_vern
          , nom_sci = the_nom_sci
          , observateur = the_observateur
          , pseudo_observer_uid = the_pseudo_observer_uid
          , oiso_code_nidif = the_oiso_code_nidif
          , oiso_statut_nidif = the_oiso_statut_nidif
          , cs_colo_repro = the_cs_colo_repro
          , cs_is_gite = the_cs_is_gite
          , cs_periode = the_cs_periode
          , nombre_total = the_nombre_total
          , code_estimation = the_code_estimation
          , date = the_date
          , date_an = the_date_an
          , altitude = the_altitude
          , mortalite = the_mortalite
          , mortalite_cause = the_mortalite_cause
          , geom = the_geom
          , exp_excl = the_exp_excl
          , code_etude = the_code_etude
          , commentaires = the_commentaires
          , pers_morale = the_pers_morale
          , derniere_maj = the_derniere_maj
          , comportement = the_comportement
          , "precision"= the_precision
          , details = the_details
          , place = the_place
          , id_formulaire = the_id_formulaire
        WHERE
            observations.source_id_data = old.id_sighting AND
            observations.source = 'dbChiroGCRA';
        RETURN new;
    END IF;
END ;
$$
    LANGUAGE plpgsql
;

DROP TRIGGER IF EXISTS obsfull_delete_from_vn_trigger ON import_vn.observations_json
;

CREATE TRIGGER obsfull_delete_from_dbchiro_trigger
    AFTER DELETE
    ON src_dbchiro.sightings
    FOR EACH ROW
EXECUTE PROCEDURE src_lpodatas.obsfull_delete_from_dbchiro()
;

DROP TRIGGER IF EXISTS obsfull_upsert_from_dbchiro_trigger ON import_vn.observations_json
;

CREATE TRIGGER obsfull_upsert_from_dbchiro_trigger
    AFTER INSERT OR UPDATE
    ON src_dbchiro.sightings
    FOR EACH ROW
EXECUTE PROCEDURE src_lpodatas.obsfull_upsert_from_dbchiro()
;

update src_dbchiro.sightings set codesp = codesp;

