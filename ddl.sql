/* Création du schéma de destination de la table fusionnée */
DROP SCHEMA IF EXISTS src_lpodatas CASCADE
;

CREATE SCHEMA src_lpodatas
;

SET search_path TO src_lpodatas,public
;

/* Création de la table des observations fusionnées */
DROP TABLE IF EXISTS src_lpodatas.observations
;



CREATE TABLE IF NOT EXISTS src_lpodatas.observations (
    id_observations     SERIAL PRIMARY KEY,
    uuid                UUID,
    source              VARCHAR(15),
    source_id_data      INT,
    source_id_sp        INT,
    taxref_cdnom        INT,
    groupe_taxo         VARCHAR(50),
    group1_inpn         VARCHAR(50),
    group2_inpn         VARCHAR(50),
    taxon_vrai          BOOL,
    nom_vern            VARCHAR(250),
    nom_sci             VARCHAR(250),
    observateur         VARCHAR(250),
    pseudo_observer_uid VARCHAR(200),
    oiso_code_nidif     INT,
    oiso_statut_nidif   VARCHAR(20),
    cs_colo_repro       BOOLEAN,
    cs_is_gite          BOOLEAN,
    cs_periode          VARCHAR(20),
    nombre_total        INT,
    code_estimation     VARCHAR(50),
    date                DATE,
    date_an             INT,
    altitude            INTEGER,
    mortalite           BOOLEAN,
    mortalite_cause     VARCHAR(250),
    geom                GEOMETRY(point, 2154),
    exp_excl            BOOLEAN,
    code_etude          VARCHAR(250),
    commentaires        TEXT,
    pers_morale         VARCHAR(100),
    comportement        TEXT[],
    "precision"         VARCHAR(50),
    details             JSONB,
    place               VARCHAR(250),
    id_formulaire       VARCHAR(20),
    derniere_maj        TIMESTAMP
)
;


CREATE UNIQUE INDEX ON src_lpodatas.observations(source, source_id_data)
;

CREATE UNIQUE INDEX ON src_lpodatas.observations(uuid)
;

CREATE INDEX ON src_lpodatas.observations(date)
;

CREATE INDEX ON src_lpodatas.observations(date_an)
;

CREATE INDEX ON src_lpodatas.observations(mortalite)
;

CREATE INDEX ON src_lpodatas.observations(mortalite_cause)
;

CREATE INDEX ON src_lpodatas.observations(altitude)
;

CREATE INDEX ON src_lpodatas.observations(taxref_cdnom)
;

CREATE INDEX ON src_lpodatas.observations(source_id_sp)
;

CREATE INDEX ON src_lpodatas.observations(source_id_data)
;

CREATE INDEX ON src_lpodatas.observations USING gist(geom)
;


INSERT INTO
    ref_nomenclatures.bib_nomenclatures_types ( mnemonique, label_default, definition_default, label_fr, definition_fr
                                              , label_en, definition_en, source, statut)
VALUES
( 'VN_ATLAS_CODE', 'Biolovision VisioNature atlas code', 'Biolovision VisioNature atlas code (sites faune-xxx.org)'
, 'Code Atlas VisioNature ', 'Code Atlas VisioNature  (sites faune-xxx.org)', 'Biolovision VisioNature atlas code'
, 'Biolovision VisioNature atlas code (sites faune-xxx.org)', 'VisioNature', TRUE)
ON CONFLICT DO NOTHING
;

INSERT INTO
    ref_nomenclatures.t_nomenclatures ( id_type, cd_nomenclature, mnemonique, label_default, definition_default
                                      , label_fr, definition_fr, source, active)
VALUES
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 0, 0
, 'Absence de code', 'Absence de code', 'Absence de code', 'Absence de code', 'VisioNature', TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 1, 1
, 'Code non valide', 'Code non valide', 'Code non valide', 'Code non valide', 'VisioNature', TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 2, 2
, 'Nicheur possible', 'Présence dans son habitat durant sa période de nidification', 'Nicheur possible'
, 'Présence dans son habitat durant sa période de nidification', 'VisioNature', TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 3, 3
, 'Nicheur possible', 'Mâle chanteur présent en période de nidification', 'Nicheur possible'
, 'Mâle chanteur présent en période de nidification', 'VisioNature', TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 4, 4
, 'Nicheur probable', 'Couple présent dans son habitat durant sa période de nidification', 'Nicheur probable'
, 'Couple présent dans son habitat durant sa période de nidification', 'VisioNature', TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 5, 5
, 'Nicheur probable', 'Comportement territorial (chant,querelles avec des voisins, etc.) observé sur un même territoire'
, 'Nicheur probable', 'Comportement territorial (chant,querelles avec des voisins, etc.) observé sur un même territoire'
, 'VisioNature', TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 6, 6
, 'Nicheur probable', 'Comportement nuptial: parades,copulation ou échange de nourriture entre adultes'
, 'Nicheur probable', 'Comportement nuptial: parades,copulation ou échange de nourriture entre adultes', 'VisioNature'
, TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 7, 7
, 'Nicheur probable', 'Visite d''un site de nidification probable. Distinct d''un site de repos', 'Nicheur probable'
, 'Visite d''un site de nidification probable. Distinct d''un site de repos', 'VisioNature', TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 8, 8
, 'Nicheur probable'
, 'Cri d''alarme ou tout autre comportement agité indiquant la présence d''un nid ou de jeunes aux alentours'
, 'Nicheur probable'
, 'Cri d''alarme ou tout autre comportement agité indiquant la présence d''un nid ou de jeunes aux alentours'
, 'VisioNature', TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 9, 9
, 'Nicheur probable'
, 'Preuve physiologique: plaque incubatrice très vascularisée ou œuf présent dans l''oviducte. Observation sur un oiseau en main'
, 'Nicheur probable'
, 'Preuve physiologique: plaque incubatrice très vascularisée ou œuf présent dans l''oviducte. Observation sur un oiseau en main'
, 'VisioNature', TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 10, 10
, 'Nicheur probable', 'Transport de matériel ou construction d''un nid; forage d''une cavité (pics)', 'Nicheur probable'
, 'Transport de matériel ou construction d''un nid; forage d''une cavité (pics)', 'VisioNature', TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 11, 11
, 'Nicheur certain'
, 'Oiseau simulant une blessure ou détournant l''attention, tels les canards, gallinacés, oiseaux de rivage,etc.'
, 'Nicheur certain'
, 'Oiseau simulant une blessure ou détournant l''attention, tels les canards, gallinacés, oiseaux de rivage,etc.'
, 'VisioNature', TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 12, 12
, 'Nicheur certain', 'Nid vide ayant été utilisé ou coquilles d''œufs de la présente saison.', 'Nicheur certain'
, 'Nid vide ayant été utilisé ou coquilles d''œufs de la présente saison.', 'VisioNature', TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 13, 13
, 'Nicheur certain'
, 'Jeunes en duvet ou jeunes venant de quitter le nid et incapables de soutenir le vol sur de longues distances'
, 'Nicheur certain'
, 'Jeunes en duvet ou jeunes venant de quitter le nid et incapables de soutenir le vol sur de longues distances'
, 'VisioNature', TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 14, 14
, 'Nicheur certain'
, 'Adulte gagnant, occupant ou quittant le site d''un nid; comportement révélateur d''un nid occupé dont le contenu ne peut être vérifié (trop haut ou dans une cavité)'
, 'Nicheur certain'
, 'Adulte gagnant, occupant ou quittant le site d''un nid; comportement révélateur d''un nid occupé dont le contenu ne peut être vérifié (trop haut ou dans une cavité)'
, 'VisioNature', TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 15, 15
, 'Nicheur certain', 'Adulte transportant un sac fécal', 'Nicheur certain', 'Adulte transportant un sac fécal'
, 'VisioNature', TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 16, 16
, 'Nicheur certain', 'Adulte transportant de la nourriture pour les jeunes durant sa période de nidification'
, 'Nicheur certain', 'Adulte transportant de la nourriture pour les jeunes durant sa période de nidification'
, 'VisioNature', TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 17, 17
, 'Nicheur certain', 'Coquilles d''œufs éclos', 'Nicheur certain', 'Coquilles d''œufs éclos', 'VisioNature', TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 18, 18
, 'Nicheur certain', 'Nid vu avec un adulte couvant', 'Nicheur certain', 'Nid vu avec un adulte couvant', 'VisioNature'
, TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 19, 19
, 'Nicheur certain', 'Nid contenant des œufs ou des jeunes (vus ou entendus)', 'Nicheur certain'
, 'Nid contenant des œufs ou des jeunes (vus ou entendus)', 'VisioNature', TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 30, 30
, 'Nicheur possible', 'Nicheur possible', 'Nicheur possible', 'Nicheur possible', 'VisioNature', TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 40, 40
, 'Nicheur probable', 'Nidification probable', 'Nicheur probable', 'Nidification probable', 'VisioNature', TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 50, 50
, 'Nicheur certain', 'Nidification certaine', 'Nicheur certain', 'Nidification certaine', 'VisioNature', TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 99, 99
, 'Espèce absente', 'Espèce absente malgré des recherches', 'Espèce absente', 'Espèce absente malgré des recherches'
, 'VisioNature', TRUE)
  ,
( (SELECT id_type FROM ref_nomenclatures.bib_nomenclatures_types WHERE mnemonique LIKE 'VN_ATLAS_CODE'), 100, 100
, 'Espèce absente', 'Espèce absente malgré des recherches', 'Espèce absente', 'Espèce absente malgré des recherches'
, 'VisioNature', TRUE)
ON CONFLICT DO NOTHING
;

CREATE TABLE IF NOT EXISTS taxonomie.cor_vn_taxref AS
SELECT row_number() OVER () AS id, vn_id, taxref_id
FROM referentiel.corresp_vn_taxref
ORDER BY
    vn_id ASC
;

DROP VIEW src_lpodatas.observateurs
;

CREATE VIEW src_lpodatas.observateurs AS
    SELECT DISTINCT
        'VisioNature' AS source
      , cast(item ->> 'id_universal' AS INTEGER) AS id_universal
      , item ->> 'name' AS nom
      , item ->> 'surname' AS prenom
      , item ->> 'email' AS email
      , item ->> 'postcode' AS code_postal
      , item ->> 'municipality' AS commune
    FROM import_vn.observers_json
    UNION
    SELECT
        'dbChiro' AS source
      , id
      , last_name
      , first_name
      , email
      , addr_city_code
      , addr_city
    FROM src_dbchiro.observers
    WHERE is_active
;