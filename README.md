# src_merged_data [WIP]

Dépot de scripts SQL permettant de fusionner dans une table métier les données issues des bases de données de la LPO AuRA (VisioNature, dbChiro,
 etc.).

## Table de destination

La création du schéma et de la table de destination se situe dans le fichier [`ddl.sql`](./ddl.sql)

## Données sources

### VisioNature

Données issues des portails VisioNature ([faune-xxx.org](https://fauneauvergnerhonealpes.org/observations/))de la région AuRA et importées grâce à l'application [Client_API_VN](https://framagit.org/lpo/Client_API_VN) bénévolement développée par @dthonon.

Intégration à l'aide du fichier [`data_from_vn.sql`](triggers_visionature.sql).


### dbChiro

Données issues du portails importées grâce à l'application [dbChiroWeb](https://framagit.org/dbchiro/dbchiroweb) principalement développée par @fred.perso.

Intégration à l'aide du fichier [`data_from_dbchiro.sql`](./data_from_dbchiro.sql) (à faire).